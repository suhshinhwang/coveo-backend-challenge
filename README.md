# Coveo Backend Challenge

This is my attempt at the [Coveo Backend Challenge](https://github.com/coveo/backend-coding-challenge).
The project uses Spring Boot and Kotlin. This is my first time using Spring Boot, and I have only started Kotlin
in March.

## Feature Overview

My attempt at the challenge achieves the following. For more details, see its relevant section.

- Fuzzy searching of city names (required)
- Scoring discrepancy based on latitude and longitude (required) 
- Pagination of results (by passing the `p` and `i` parameter)
- Caching (using the default `@Cacheable` annotation)

### Fuzzy Searching of city names

Suggestions based on fuzzy searching is done in two steps and a scoring step. Once both steps are complete, the 
result is forwarded to the controller and is converted to a list of suggestions. A limitation on the fuzzy search is 
that the user must input the city name, then if he/she wishes can use commas to add administration 1 or 2 names. 
(Refer to the `Choices and Decisions` section for the reason)
 
The first step provides cities based on only city name. This is done by retrieving city names that have an edit 
distance (Levenshtein edit distance) less than or equal to a dynamic threshold.

The second scoring step is done by calculating the edit distance of the query to each of the cities provided in step 1.
For the additional administration 1 or 2 names, they are scored as follows:

1. For each of the city, create a list of strings C where the content is: `name`, `admin1name`, `admin2name` (in 
order). The country name is left out.
1. The query string is split by commas. Let this list be called `Q`.
1. `Q[0]` is compared to the `name` (`C[0]`)
1. `Q[1]` and all elements after are compared to `C[1]` and after to find the maximum. If the maximum does not occur 
at the index of comparison, then all future comparisons are shifted. This allows the following case: Assume a city of 
"City, Admin1, Admin2". A query of `City, Admin1` should have the same score as a query of `City, Admin2`. The 
algorithm is flexible such that if there were `Admin3` or `Admin4`, then a query of `City, Admin3` and `City, Admin4`
 would yield the same results.
 
The limitation is that the "Tolerance of Ambiguity" only goes forward and not backward. Thus `City, Admin2, Admin1` 
would yield a lower score than `City, Admin1, Admin2`.

### Scoring discrepancy based on location

As required, two parameters `lat` and `long` are provided so location information can be used to re-order the 
suggestions list. When either or both of the parameters are present, the calculated fuzzy score is subtracted by a 
distance weight (a number between 0 and 1, inclusively). The distant weighting is calculated as follows:

1. All the distances between the city and query location are computed.
1. For the closest distance, it receives a score of 0. For the furthest, it receives a 1.
1. All the other distances receive a score that are scaled linearly with respect to the closest and furthest score.
1. The weight is calculated by multiplying the distance weight by each of the distance scores.  

### Pagination

Since the results of fuzzy searching may be very numerable and the front end use case unknown, I think it is preferable 
that there is pagination. This accounts for several use cases: a dropdown in a search text field where it is not useful 
to provide a list of more than 10 elements, and a general list where all results can be shown or paginated.
 
By default, if the page parameter `p` is not provided, all the result is shown. If `p` is present but not `i`, then 
it defaults to showing 10 elements. 

### Caching

The default `@Cacheable` annotation is used to provide simple caching. The motive behind the cache is to allow quick 
access to pagination in the previous step.

## Limitations
The following limitations occur

- Did not include admin code 3 for US cities for data consistency.
- Did not allow users to query for countries (they are stored in the database as US and CA).
- Did not allow users to search using short name for State/Provinces.
- Did not include other names of a city.
- Will need to restart server if there are changes to the `cities` table since there is no method implemented to 
invalidate the cache.
- Users can only search using city names. Searching a province name or state name does not list out all cities in them.

## Choices and Decisions
The following choices and decisions were made.

- Folders were used even though many only contained one file. This sets up structure for future additions.
- Chose to load BK tree in memory since this can reduce overhead to connecting the DB.
- Chose to use Apache Commons for Edit Distance and an existing BK Tree implementation because I did 
not want to reinvent the wheel.
- Preferred flexibility and adherence to architecture over performance.
- Attempted with multiple search keys but did not pursue since complexity is very high
- Used a coded API Errors so that frontend can easily determine what message to display

## Running Tests
In order to run tests, you will need to provide your own `test.properties` under the `test` directory and provide the
 correct environment variables.