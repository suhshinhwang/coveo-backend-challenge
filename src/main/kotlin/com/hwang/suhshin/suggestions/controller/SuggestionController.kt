package com.hwang.suhshin.suggestions.controller

import com.hwang.suhshin.suggestions.api.model.response.Suggestion
import com.hwang.suhshin.suggestions.controller.utility.SuggestionScoreComparator
import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.LookupQuery
import com.hwang.suhshin.suggestions.model.exception.InvalidRangeException
import com.hwang.suhshin.suggestions.model.service.suggestion.Score
import com.hwang.suhshin.suggestions.model.service.suggestion.SuggestedCitiesProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class SuggestionController @Autowired constructor(private val suggestedCitiesProvider: SuggestedCitiesProvider) {
  @RequestMapping("/suggestions", method = [RequestMethod.GET])
  fun suggestions(
      @RequestParam("q") name: String,
      @RequestParam("lat", required = false) latitude: Double?,
      @RequestParam("long", required = false) longitude: Double?,
      @RequestParam("p", required = false) page: Int? = null,
      @RequestParam("i", required = false) itemsPerPage: Int? = null
  ): List<Suggestion> {
    val query = buildQueryFromParams(name, latitude, longitude)
    val suggestions = suggestedCitiesProvider.getSuggestedCities(query)
        .map(::buildSuggestionFromCityScoreMap)
        .sortedWith(SuggestionScoreComparator())
        .reversed()

    return paginateResults(suggestions, page, itemsPerPage)
  }
}

private fun buildSuggestionFromCityScoreMap(entry: Map.Entry<City, Score>): Suggestion {
  val city = entry.key
  val score = entry.value
  return Suggestion(city.fullName, city.latitude, city.longitude, score)
}

private fun buildQueryFromParams(name: String, latitude: Double?, longitude: Double?): LookupQuery {
  return LookupQuery(name, latitude, longitude)
}

private fun <T> paginateResults(collection: List<T>, page: Int? = null, itemsPerPage: Int?): List<T> {
  if (page == null) return collection

  val _itemsPerPage = itemsPerPage ?: 10

  if (page < 0 || _itemsPerPage < 0) throw InvalidRangeException("Pagination parameters must be non-negative")

  val subListStartIndex = page * _itemsPerPage
  if (collection.isEmpty() || subListStartIndex >= collection.size) return emptyList()

  val subListEndIndex = if (subListStartIndex + _itemsPerPage > collection.size)
    collection.size
  else
    subListStartIndex + _itemsPerPage

  return collection.subList(subListStartIndex, subListEndIndex)
}