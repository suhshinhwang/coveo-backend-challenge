package com.hwang.suhshin.suggestions.controller.handler

import com.hwang.suhshin.suggestions.api.model.response.ApiError
import com.hwang.suhshin.suggestions.api.model.response.BadRequestCodes
import com.hwang.suhshin.suggestions.model.exception.InvalidInputException
import com.hwang.suhshin.suggestions.model.exception.InvalidRangeException
import org.springframework.beans.TypeMismatchException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.HttpRequestMethodNotSupportedException
import org.springframework.web.bind.MissingServletRequestParameterException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class RestExceptionHandler : ResponseEntityExceptionHandler() {

  @ExceptionHandler(IllegalArgumentException::class)
  fun handleBadRequestExceptions(exception: Exception): ResponseEntity<ApiError> {
    return ResponseEntity(mapExceptionsToApiError(exception), HttpStatus.BAD_REQUEST)
  }

  override fun handleHttpRequestMethodNotSupported(exception: HttpRequestMethodNotSupportedException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
    return replyWith("Method is not supported.", HttpStatus.METHOD_NOT_ALLOWED)
  }

  override fun handleMissingServletRequestParameter(exception: MissingServletRequestParameterException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
    val requiredParameterName = exception.parameterName
    val requiredParameterType = exception.parameterType

    val errorMessage = "Parameter [$requiredParameterName] of type [$requiredParameterType] is required."
    return replyWith(errorMessage, HttpStatus.BAD_REQUEST)
  }

  override fun handleTypeMismatch(exception: TypeMismatchException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
    return ResponseEntity(createTypeApiError(exception), HttpStatus.BAD_REQUEST)
  }
}

private fun replyWith(message: String, httpStatus: HttpStatus): ResponseEntity<Any> {
  return ResponseEntity(ApiError(message), httpStatus)
}

private fun mapExceptionsToApiError(exception: Exception): ApiError {
  var errorCode: Int? = null

  when (exception) {
    is InvalidInputException -> errorCode = BadRequestCodes.INVALID_CHARACTERS.code
    is InvalidRangeException -> errorCode = BadRequestCodes.INVALID_RANGE.code
  }

  return ApiError(exception.message, errorCode)
}

private fun createTypeApiError(exception: TypeMismatchException): ApiError {
  if (exception is MethodArgumentTypeMismatchException) {
    return createMethodArgumentErrorMessage(exception)
  }

  return ApiError("Parameter type is incorrect, please check again.")
}

private fun createMethodArgumentErrorMessage(exception: MethodArgumentTypeMismatchException): ApiError {

  val parameterError = String.format("Type for [%s] is incorrect.", exception.name)
  val expectedType = mapClassToString(exception.requiredType)

  var typeError = ""
  if (expectedType != null) {
    typeError = " It is expecting [$expectedType]"
  }

  val errorMessage = "$parameterError$typeError"
  return ApiError(errorMessage)
}

private fun mapClassToString(clazz: Class<*>?): String? {
  if (clazz == null) return null
  when (clazz) {
    Integer::class.java -> return "integer"
    String::class.java -> return "string"
  }
  return null
}