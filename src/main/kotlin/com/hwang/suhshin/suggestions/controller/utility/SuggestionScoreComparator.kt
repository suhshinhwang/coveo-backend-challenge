package com.hwang.suhshin.suggestions.controller.utility

import com.hwang.suhshin.suggestions.api.model.response.Suggestion

class SuggestionScoreComparator : Comparator<Suggestion> {
  override fun compare(suggestionA: Suggestion, suggestionB: Suggestion): Int {
    if (suggestionA.score > suggestionB.score) return 1
    if (suggestionA.score < suggestionB.score) return -1
    return 0
  }
}