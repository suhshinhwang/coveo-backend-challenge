package com.hwang.suhshin.suggestions.model.domain

import javax.persistence.Id

typealias CityId = Int

data class City(
    @Id val id: CityId,
    val name: String,
    val latitude: Double,
    val longitude: Double,
    val country: String,
    val admin1name: String? = null,
    val admin2name: String? = null
) {
  val fullName = buildFullName(this)
}

private fun buildFullName(city: City): String {
  if (city.admin1name?.isEmpty() == null && city.admin2name?.isEmpty() == null) {
    return String.format("%s, %s", city.name, city.country)
  } else if (city.admin1name?.isEmpty() == null) {
    return String.format("%s, %s, %s", city.name, city.admin2name, city.country)
  } else if (city.admin2name?.isEmpty() == null) {
    return String.format("%s, %s, %s", city.name, city.admin1name, city.country)
  }
  return String.format("%s, %s, %s, %s", city.name, city.admin2name, city.admin1name, city.country)
}