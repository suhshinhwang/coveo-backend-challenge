package com.hwang.suhshin.suggestions.model.service.suggestion.scoring

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan("com.hwang.suhshin.suggestions.model.service.suggestion.scoring")
class Configuration {

  @Bean
  fun simpleEditDistanceScoresCalculator(): WeightedEditDistanceScoresCalculator {
    return WeightedEditDistanceScoresCalculator(0.1, 0.2)
  }
}