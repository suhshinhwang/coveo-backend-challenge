package com.hwang.suhshin.suggestions.model.repository

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.repository.entity.CityEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class JpaCityRepository(@Autowired private val cityRepository: CityRepository) : CityClient {
  override fun findAll(): List<City> {
    return cityRepository.findAll().toList().map { CityEntity.toDomainModel(it) }
  }
}
