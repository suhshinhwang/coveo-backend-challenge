package com.hwang.suhshin.suggestions.model.service.suggestion

import edu.gatech.gtri.bktree.BkTreeSearcher
import edu.gatech.gtri.bktree.Metric
import edu.gatech.gtri.bktree.MutableBkTree

class BkTree<T>(items: Collection<T>, metric: Metric<T>) {
  private val bkTree: BkTreeSearcher<T> = generateInitialBkTree(items, metric)

  fun search(query: T, distance: Int): Collection<T> {
    val matches = bkTree.search(query, distance)

    return matches.map { it.match }
  }
}

private fun <T> generateInitialBkTree(indices: Collection<T>, metric: Metric<T>): BkTreeSearcher<T> {
  val bkTree = MutableBkTree<T>(metric)
  bkTree.addAll(indices)
  return BkTreeSearcher(bkTree)
}
