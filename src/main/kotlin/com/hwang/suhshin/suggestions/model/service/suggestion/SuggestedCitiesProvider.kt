package com.hwang.suhshin.suggestions.model.service.suggestion

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.LookupQuery

typealias Score = Double

interface SuggestedCitiesProvider {
  fun getSuggestedCities(query: LookupQuery): Map<City, Score>
}