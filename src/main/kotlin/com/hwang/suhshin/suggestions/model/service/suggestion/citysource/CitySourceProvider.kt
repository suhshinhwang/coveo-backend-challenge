package com.hwang.suhshin.suggestions.model.service.suggestion.citysource

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.LookupQuery

interface CitySourceProvider {
  fun provide(query: LookupQuery): Collection<City>
}