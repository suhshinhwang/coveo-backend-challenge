package com.hwang.suhshin.suggestions.model.service.suggestion.scoring

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.CityId
import com.hwang.suhshin.suggestions.model.domain.LookupQuery
import com.hwang.suhshin.suggestions.model.service.suggestion.Score
import com.hwang.suhshin.suggestions.model.utility.Distance
import com.hwang.suhshin.suggestions.model.utility.Point
import com.hwang.suhshin.suggestions.model.utility.normalizeString
import com.hwang.suhshin.suggestions.model.utility.splitByCommas
import org.apache.commons.text.similarity.LevenshteinDistance
import kotlin.math.abs
import kotlin.math.hypot

class WeightedEditDistanceScoresCalculator(
    private val distanceScoreScale: Double,
    private val bonusWeight: Double
) : QueryScoresCalculator {
  init {
    if (distanceScoreScale < 0 || distanceScoreScale > 1.0)
      throw IllegalArgumentException("Distance Score scaling must be between 0 to 1 inclusively.")
    if (bonusWeight < 0 || bonusWeight > 1.0)
      throw IllegalArgumentException("Bonus weight must be between 0 to 1 inclusively.")
  }

  override fun computeScores(lookupQuery: LookupQuery, cities: Collection<City>): Map<City, Score> {
    val distanceScoreModifier = createDistanceScoreModifier(lookupQuery.latitude, lookupQuery.longitude, cities)

    val cityScoreMap = mutableMapOf<City, Score>()
    cities.forEach {
      val nameScore = computeScore(lookupQuery, it, bonusWeight)
      val distanceScore = (distanceScoreModifier[it.id] ?: 0.0) * distanceScoreScale
      cityScoreMap[it] = nameScore - distanceScore
    }
    val maxAddedScore = cityScoreMap.maxBy { it.value }?.value ?: 1.0
    if (maxAddedScore > 1.0) {
      cityScoreMap.forEach { city, score ->
        cityScoreMap[city] = score / maxAddedScore
      }
    }

    return cityScoreMap.toMap()
  }
}

private val levenshteinDistance = LevenshteinDistance()

/**
 * Internal for testing
 */
internal fun computeScore(lookupQuery: LookupQuery, city: City, bonusWeight: Double): Score {
  val normalizedQueryStrings = splitByCommas(lookupQuery.name).map(::normalizeString)
  if (normalizedQueryStrings.isEmpty()) return 0.0

  val normalizedCityNames = getCityNames(city).map(::normalizeString)
  val cityNameScore = computeSimilarityScore(normalizedQueryStrings[0], normalizedCityNames[0])
  var bonus = 0.0
  var indexShiftCount = 1
  if (normalizedQueryStrings.size > 1) {
    bonus = normalizedQueryStrings.subList(1, normalizedQueryStrings.size).mapIndexed { index, subquery ->
      val shiftedIndex = index + indexShiftCount
      if (normalizedCityNames.size > shiftedIndex) {
        val similarityScores = normalizedCityNames
            .subList(shiftedIndex, normalizedCityNames.size)
            .map { computeSimilarityScore(it, subquery) }
        val maxScore = similarityScores.max()!! // Guaranteed to have at least 1 element
        val maxIndex = similarityScores.indexOf(maxScore)
        if (maxIndex > 0) indexShiftCount += maxIndex
        return@mapIndexed maxScore
      }
      return@mapIndexed 0.0
    }.sum()
  }

  return cityNameScore * (1 - bonusWeight) + bonus * bonusWeight
}

/**
 * Creates a list of city names (city name, admin 1 name, admin 2 name).
 * Does **NOT** include country name.
 */
internal fun getCityNames(city: City): List<String> {
  val normalizedNames = mutableListOf(city.name)
  if (city.admin1name != null) {
    normalizedNames.add(city.admin1name)
  }
  if (city.admin2name != null) {
    normalizedNames.add(city.admin2name)
  }
  return normalizedNames.toList()
}

private fun computeSimilarityScore(stringA: String, stringB: String): Double {
  if (stringA.isEmpty() || stringB.isEmpty()) return 1.0
  val longestLength = maxOf(stringA.length, stringB.length)

  val editDistance = levenshteinDistance.apply(stringA, stringB)
  return 1.0 - (editDistance.toDouble() / longestLength)
}

/**
 * For each suggested city, returns a value inclusively between 0.0 to 1.0.
 * 0.0 indicates that this city has the closest distance to the query and
 * 1.0 indicating the furthest distance.
 */
private fun createDistanceScoreModifier(
    queryX: Double?,
    queryY: Double?,
    suggestedCities: Collection<City>
): Map<CityId, Double> {
  if (suggestedCities.isEmpty()) return mapOf()
  if (suggestedCities.size == 1) return mapOf(Pair(suggestedCities.iterator().next().id, 0.0))
  if (queryX == null && queryY == null) {
    return suggestedCities.map { Pair(it.id, 0.0) }.toMap()
  }

  val distanceBetweenQueryAndCities = computeDistancesBetweenQueryAndCities(queryX, queryY, suggestedCities)
  val queryAndCityDistances = distanceBetweenQueryAndCities.values.toList()

  // The `min` and `max` methods cannot be null because `computeDistancesBetweenQueryAndCities` will always
  // return a length of at least 1 due to the checks in the beginning of this method.
  val minDistance = queryAndCityDistances.min()!!
  val distanceRange = queryAndCityDistances.max()!! - minDistance

  val modifierMap = mutableMapOf<CityId, Double>()
  distanceBetweenQueryAndCities.forEach { cityId: CityId, distance: Distance ->
    modifierMap[cityId] = (distance - minDistance) / distanceRange
  }
  return modifierMap.toMap()
}

private fun computeDistancesBetweenQueryAndCities(
    queryX: Double?,
    queryY: Double?,
    suggestedCities: Collection<City>): Map<CityId, Distance> {
  return suggestedCities.map {
    val distance = computeNullablePointDistance(queryX, queryY, Point(it.latitude, it.longitude))
    return@map Pair(it.id, distance)
  }.toMap()
}

private fun computeNullablePointDistance(x: Double?, y: Double?, point: Point): Double {
  if (x == null && y == null) return 0.0
  if (x == null && y != null) {
    return abs(y - point.y)
  }
  if (y == null && x != null) {
    return abs(x - point.x)
  }
  return hypot((x!! - point.x), (y!! - point.y))
}