package com.hwang.suhshin.suggestions.model.utility

import java.text.Normalizer
import java.util.regex.Pattern

private val findNoneDelimiterText = Pattern.compile("[^\\s,]+")

private val spaceRegex = Regex("\\s+")

private val accentRegex = Regex("[^\\p{ASCII}]")


fun removeAccent(input: String): String {
  return Normalizer
      .normalize(input, Normalizer.Form.NFD).replace(accentRegex, "")
}

fun normalizeString(input: String): String {
  return removeAccent(input).toLowerCase().replace(spaceRegex, " ").trim()
}

fun splitBySpaceAndCommas(input: String): List<String> {
  val matcher = findNoneDelimiterText.matcher(input)
  val output = mutableListOf<String>()
  while (matcher.find()) {
    output.add(matcher.group())
  }
  return output.toList()
}

fun splitByCommas(input: String): List<String> {
  return input.split(',').filter { !it.isEmpty() }
}