package com.hwang.suhshin.suggestions.model.service.suggestion

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.LookupQuery
import com.hwang.suhshin.suggestions.model.service.suggestion.citysource.CitySourceProvider
import com.hwang.suhshin.suggestions.model.service.suggestion.scoring.QueryScoresCalculator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Component

@Component
class DefaultSuggestedCitiesProvider @Autowired constructor(
    private val citySourceProvider: CitySourceProvider,
    private val queryScoresCalculator: QueryScoresCalculator
) : SuggestedCitiesProvider {

  @Cacheable("suggestedCities")
  override fun getSuggestedCities(query: LookupQuery): Map<City, Score> {
    val suggestedCities = citySourceProvider.provide(query)
    return queryScoresCalculator.computeScores(query, suggestedCities)
  }
}
