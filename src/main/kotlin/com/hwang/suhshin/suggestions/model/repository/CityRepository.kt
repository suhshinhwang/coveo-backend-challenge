package com.hwang.suhshin.suggestions.model.repository

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.repository.entity.CityEntity
import org.springframework.data.repository.CrudRepository

interface CityRepository : CrudRepository<CityEntity, Int>

interface CityClient {
  fun findAll(): List<City>
}