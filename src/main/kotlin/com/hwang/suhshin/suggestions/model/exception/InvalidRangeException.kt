package com.hwang.suhshin.suggestions.model.exception

data class InvalidRangeException(override val message: String?) : IllegalArgumentException(message)