package com.hwang.suhshin.suggestions.model.service.suggestion.scoring

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.LookupQuery
import com.hwang.suhshin.suggestions.model.service.suggestion.Score

interface QueryScoresCalculator {
  fun computeScores(lookupQuery: LookupQuery, cities: Collection<City>): Map<City, Score>
}