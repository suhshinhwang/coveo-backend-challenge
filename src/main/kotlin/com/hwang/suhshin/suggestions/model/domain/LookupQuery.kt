package com.hwang.suhshin.suggestions.model.domain

data class LookupQuery(
    val name: String,
    val latitude: Double? = null,
    val longitude: Double? = null
)