package com.hwang.suhshin.suggestions.model.service.suggestion.citysource

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.utility.normalizeString
import edu.gatech.gtri.bktree.Metric
import org.apache.commons.text.similarity.LevenshteinDistance


class LevenshteinCityNameMetric : Metric<City> {
  private val levenshteinDistance = LevenshteinDistance()

  override fun distance(x: City?, y: City?): Int {
    val xName = normalizeString(x?.name ?: "")
    val yName = normalizeString(y?.name ?: "")
    return levenshteinDistance.apply(xName, yName)
  }
}