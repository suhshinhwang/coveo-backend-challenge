package com.hwang.suhshin.suggestions.model.service.suggestion.citysource

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.LookupQuery
import com.hwang.suhshin.suggestions.model.exception.InvalidInputException
import com.hwang.suhshin.suggestions.model.repository.CityClient
import com.hwang.suhshin.suggestions.model.service.suggestion.BkTree
import com.hwang.suhshin.suggestions.model.utility.normalizeString
import com.hwang.suhshin.suggestions.model.utility.splitByCommas
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

private val logger = LoggerFactory.getLogger(DefaultCityProvider::class.java)

private val acceptedCharactersRegex = Regex("[A-Za-z\\-\\ ,]+")


@Component
class DefaultCityProvider(cityClient: CityClient) : CitySourceProvider {
  private val bkTree: BkTree<City>

  init {
    logger.info("Initializing BK Tree with cities from database.")
    bkTree = BkTree(cityClient.findAll(), LevenshteinCityNameMetric())
  }

  override fun provide(query: LookupQuery): Collection<City> {
    val trimmedQueryName = query.name.trim()
    if (trimmedQueryName.isEmpty()) return emptyList()
    val normalizedString = normalizeString(trimmedQueryName)
    validateInput(query.name)

    val firstQueryBeforeComma = splitByCommas(trimmedQueryName).map(::normalizeString)[0]

    val queryCity = buildQueryCity(LookupQuery(firstQueryBeforeComma, query.latitude, query.longitude))
    return bkTree.search(queryCity, limitTo(normalizedString.length, 5))
  }
}

private fun validateInput(input: String) {
  val normalizedString = normalizeString(input)
  if (!isValidInput(normalizedString))
    throw InvalidInputException("Can only accept alphabets, dash, spaces and commas as query.")
}

internal fun isValidInput(input: String): Boolean {
  if (input.isEmpty()) return false
  return acceptedCharactersRegex.matchEntire(input) != null
}

private fun buildQueryCity(query: LookupQuery): City {
  return City(
      id = 0,
      name = normalizeString(query.name),
      country = "",
      latitude = 0.0,
      longitude = 0.0
  )
}

private fun limitTo(input: Int, limit: Int): Int {
  if (input > limit) return limit
  return input
}