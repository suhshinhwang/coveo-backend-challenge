package com.hwang.suhshin.suggestions.model.repository.entity

import com.hwang.suhshin.suggestions.model.domain.City
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "cities")
data class CityEntity(
    @Id val id: Int,
    val name: String,
    val latitude: Double,
    val longitude: Double,
    val country: String,
    val admin1name: String?,
    val admin2name: String?
) {
  companion object {
    fun toDomainModel(cityEntity: CityEntity): City {
      return City(
          cityEntity.id,
          cityEntity.name,
          cityEntity.latitude,
          cityEntity.longitude,
          cityEntity.country,
          cityEntity.admin1name,
          cityEntity.admin2name
      )
    }
  }
}