package com.hwang.suhshin.suggestions.model.utility

typealias Distance = Double

data class Point(
    val x: Double,
    val y: Double
)