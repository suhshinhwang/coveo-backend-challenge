package com.hwang.suhshin.suggestions.model.exception

data class InvalidInputException(override val message: String?) : IllegalArgumentException(message)