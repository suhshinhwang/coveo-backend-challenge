package com.hwang.suhshin.suggestions.api.model.response

data class Suggestion(
    val name: String,
    val latitude: Double,
    val longitude: Double,
    val score: Double
)
