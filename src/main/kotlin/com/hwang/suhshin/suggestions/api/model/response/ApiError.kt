package com.hwang.suhshin.suggestions.api.model.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ApiError(
    val message: String?,
    val code: Int? = null
)