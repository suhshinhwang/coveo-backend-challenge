package com.hwang.suhshin.suggestions.api.model.response

enum class BadRequestCodes constructor(val code: Int) {
  INVALID_CHARACTERS(1),
  INVALID_RANGE(2)
}