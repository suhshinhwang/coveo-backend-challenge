package com.hwang.suhshin.suggestions

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SuggestionsApplication

fun main(args: Array<String>) {
  runApplication<SuggestionsApplication>(*args)
}
