package com.hwang.suhshin.suggestions.controller.utility

import com.hwang.suhshin.suggestions.api.model.response.Suggestion
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

internal class SuggestionScoreComparatorTest {

  private val suggestionScore_5 = Suggestion("city1", 0.0, 0.0, 0.5)
  private val suggestionScore_7 = Suggestion("city1", 0.0, 0.0, 0.7)
  private val suggestionScore_8 = Suggestion("city1", 0.0, 0.0, 0.8)

  private val suggestions = listOf(suggestionScore_5, suggestionScore_8, suggestionScore_7)

  @Test
  fun comparisonTest() {
    val sortedSuggestions = suggestions.sortedWith(SuggestionScoreComparator())
    assertThat(sortedSuggestions).containsExactly(suggestionScore_5, suggestionScore_7, suggestionScore_8)
  }
}