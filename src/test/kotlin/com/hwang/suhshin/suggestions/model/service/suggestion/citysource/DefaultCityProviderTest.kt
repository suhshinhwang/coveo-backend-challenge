package com.hwang.suhshin.suggestions.model.service.suggestion.citysource

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.LookupQuery
import com.hwang.suhshin.suggestions.model.exception.InvalidInputException
import com.hwang.suhshin.suggestions.model.repository.CityClient
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class DefaultCityProviderTest {

  private lateinit var mockedCityClient: CityClient
  private lateinit var cityProvider: CitySourceProvider

  @Before
  fun initialize() {
    val mockedCityClient = mock(CityClient::class.java)
    `when`(mockedCityClient.findAll()).thenReturn(mockedCities)
    cityProvider = DefaultCityProvider(mockedCityClient)
  }

  @Test
  fun testNormalLookup() {
    assertThat(cityProvider.provide(LookupQuery("")).map { it.name })
        .isEmpty()
    assertThat(cityProvider.provide(LookupQuery("city")).map { it.name })
        .containsExactlyInAnyOrder("City 1", "City 2")
  }

  @Test
  fun testBadQueries() {
    val thrownException = catchThrowable {
      cityProvider.provide(LookupQuery("bad*query"))
    }

    assertThat(thrownException).isInstanceOf(InvalidInputException::class.java)
  }
}

private val mockedCities = listOf(
    City(
        1,
        "City 1",
        0.0,
        0.0,
        "Country"
    ),
    City(
        1,
        "City 2",
        0.0,
        0.0,
        "Country"
    )
)