package com.hwang.suhshin.suggestions.model.service.suggestion

import edu.gatech.gtri.bktree.Metric
import org.apache.commons.text.similarity.LevenshteinDistance
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

internal class BkTreeTest {

  private lateinit var bkTree: BkTree<String>

  @Before
  fun initializeBkTree() {
    val searchTerms = listOf("hello", "jello", "fello", "fellow", "banana", "banane", "banan")
    bkTree = BkTree(searchTerms, TestingStringMetric())
  }

  @Test
  fun testSearch() {
    assertThat(bkTree.search("jell", 0)).isEmpty()
    assertThat(bkTree.search("jell", 1)).containsExactly("jello")
    assertThat(bkTree.search("hello", 0)).containsExactly("hello")
    assertThat(bkTree.search("hello", 1)).containsExactlyInAnyOrder("hello", "jello", "fello")
    assertThat(bkTree.search("hello", 2)).containsExactlyInAnyOrder("hello", "jello", "fello", "fellow")
    assertThat(bkTree.search("anan", 2)).containsExactlyInAnyOrder("banana", "banane", "banan")
  }
}

private class TestingStringMetric : Metric<String> {
  private val levenshteinDistance = LevenshteinDistance()
  override fun distance(x: String?, y: String?): Int {
    return levenshteinDistance.apply(x, y)
  }

}