package com.hwang.suhshin.suggestions.model.utility

import com.hwang.suhshin.suggestions.model.service.suggestion.citysource.isValidInput
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


internal class StringKtTest {
  @Test
  fun removeAccentTest() {
    assertThat(removeAccent("ÈÉÊËÛÙÏÎÀÂÔèéêëûùïîàâôÇçÃãÕõ")).isEqualTo("EEEEUUIIAAOeeeeuuiiaaoCcAaOo")
    assertThat(removeAccent("çÇáéíóúýÁÉÍÓÚÝàèìòùÀÈÌÒÙãõñäëïöüÿÄËÏÖÜÃÕÑâêîôûÂÊÎÔÛ"))
        .isEqualTo("cCaeiouyAEIOUYaeiouAEIOUaonaeiouyAEIOUAONaeiouAEIOU")
  }

  @Test
  fun normalizeStringTest() {
    assertThat(normalizeString("ÈÉÊËÛÙÏÎÀÂÔèéêëûùïîàâôÇçÃãÕõ")).isEqualTo("eeeeuuiiaaoeeeeuuiiaaoccaaoo")
    assertThat(normalizeString("çÇáéíóúýÁÉÍÓÚÝàèìòùÀÈÌÒÙãõñäëïöüÿÄËÏÖÜÃÕÑâêîôûÂÊÎÔÛ"))
        .isEqualTo("ccaeiouyaeiouyaeiouaeiouaonaeiouyaeiouaonaeiouaeiou")
    assertThat(normalizeString("óúýÁ ëïöü")).isEqualTo("ouya eiou")
    assertThat(normalizeString("óúýÁ    ëïöü")).isEqualTo("ouya eiou")
    assertThat(normalizeString("   óúýÁ  ëïöü ")).isEqualTo("ouya eiou")
  }

  @Test
  fun splitByCommasTest() {
    assertThat(splitByCommas("a")).containsExactly("a")
    assertThat(splitByCommas("a,b")).containsExactly("a", "b")
    assertThat(splitByCommas("a,")).containsExactly("a")
    assertThat(splitByCommas(",a")).containsExactly("a")
    assertThat(splitByCommas("a,  b")).containsExactly("a", "  b")
  }

  @Test
  fun isPrintableAsciiTest() {
    assertThat(isValidInput("")).isFalse()
    assertThat(isValidInput("abc def")).isTrue()
    assertThat(isValidInput("abc-def")).isTrue()
    assertThat(isValidInput("abc,def")).isTrue()
    assertThat(isValidInput("abc_def")).isFalse()
    assertThat(isValidInput("Montréal")).isFalse()
    assertThat(isValidInput("hell_o")).isFalse()
    assertThat(isValidInput("中文")).isFalse()
  }

  @Test
  fun addressSpacerRegexTest() {
    testSplittingBySpaceAndCommas("中文", "中文")
    testSplittingBySpaceAndCommas("中 文", "中", "文")
    testSplittingBySpaceAndCommas("Montréal", "Montréal")
    testSplittingBySpaceAndCommas("Mon tréal", "Mon", "tréal")
    testSplittingBySpaceAndCommas("one-text", "one-text")
    testSplittingBySpaceAndCommas("one-text", "one-text")
    testSplittingBySpaceAndCommas("two text", "two", "text")
    testSplittingBySpaceAndCommas("two,text", "two", "text")
    testSplittingBySpaceAndCommas("two ,text", "two", "text")
    testSplittingBySpaceAndCommas("two , text", "two", "text")
    testSplittingBySpaceAndCommas("two   , text", "two", "text")
    testSplittingBySpaceAndCommas("   two text   ", "two", "text")
  }
}

private fun testSplittingBySpaceAndCommas(input: String, vararg expectedStrings: String) {
  val splitted = splitBySpaceAndCommas(input)
  assertThat(splitted.size).isEqualTo(expectedStrings.size)
  assertThat(splitted).containsExactly(*expectedStrings)
}
