package com.hwang.suhshin.suggestions.model.repository.entity

import com.hwang.suhshin.suggestions.model.domain.City
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

internal class CityEntityTest {
  @Test
  fun toDomainModelTest(){
    val cityEntity = CityEntity(
        id = 1,
        name = "City",
        latitude = 1.23,
        longitude = 3.21,
        country = "Here",
        admin1name = "admin1",
        admin2name = "admin2"
    )
    val domainCity = City(
        id = 1,
        name = "City",
        latitude = 1.23,
        longitude = 3.21,
        country = "Here",
        admin1name = "admin1",
        admin2name = "admin2"
    )
    assertThat(CityEntity.toDomainModel(cityEntity)).isEqualTo(domainCity)
  }
}
