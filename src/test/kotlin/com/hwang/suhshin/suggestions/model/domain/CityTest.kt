package com.hwang.suhshin.suggestions.model.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

internal class CityTest {
  @Test
  fun testFullNameCreation() {
    val city1 = City(0,
        "City1",
        0.0,
        0.0,
        admin1name = "admin1",
        admin2name = "admin2",
        country = "country")
    assertThat(city1.fullName).isEqualTo("City1, admin2, admin1, country")

    val city2 = City(0,
        "City2",
        0.0,
        0.0,
        admin2name = "admin2",
        country = "country")

    assertThat(city2.fullName).isEqualTo("City2, admin2, country")

    val city3 = City(0,
        "City3",
        0.0,
        0.0,
        admin1name = "admin1",
        country = "country")

    assertThat(city3.fullName).isEqualTo("City3, admin1, country")

    val city4 = City(0,
        "City4",
        0.0,
        0.0,
        country = "country")

    assertThat(city4.fullName).isEqualTo("City4, country")
  }
}