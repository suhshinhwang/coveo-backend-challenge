package com.hwang.suhshin.suggestions.model.service.suggestion.scoring

import com.hwang.suhshin.suggestions.model.domain.City
import com.hwang.suhshin.suggestions.model.domain.LookupQuery
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


internal class WeightedEditDistanceScoresCalculatorTest {

  private val scoresCalculator = WeightedEditDistanceScoresCalculator(0.5, 0.2)

  @Test
  fun testScoreWithOnlyName() {
    assertNameOrder("city", "City", "City", "City of Angels", "Machi")
    assertNameOrder("city of angels", "City of Angels", "City", "City", "Machi")
    assertNameOrder("cty", "City", "City", "City of Angels", "Machi")
    assertNameOrder("ity", "City", "City", "City of Angels", "Machi")
    assertNameOrder("ct", "City", "City", "Machi", "City of Angels")
    assertNameOrder("c", "City", "City", "Machi", "City of Angels")
    assertNameOrder("ct o ang", "City of Angels", "City", "City", "Machi")
    assertNameOrder("of angel", "City of Angels", "Machi", "City", "City")
    assertNameOrder("angels", "City of Angels", "City", "City", "Machi")
    assertNameOrder("machi", "Machi", "City of Angels", "City", "City")
    assertNameOrder("mai", "Machi", "City of Angels", "City", "City")
  }

  @Test
  fun testScoreWithCompoundNames() {
    assertCompoundNameOrder("city, where",
        "City, Where, There",
        "City, Nowhere, Here",
        "City, Somewhere",
        "City, Above",
        "Citi"
    )

    assertCompoundNameOrder("city, smew",
        "City, Somewhere",
        "City, Where, There",
        "City, Nowhere, Here",
        "City, Above",
        "Citi"
    )
  }

  @Test
  fun testAmbiguousSingleQueryForCompoundNames() {
    //orderedCityFieldsFromQuery
    val orderedCityNames = orderedCityFieldsFromQuery(
        mockedCitiesForCompoundNames,
        LookupQuery("city"),
        { getCityNames(it).joinToString(", ") }
    )

    assertThat(orderedCityNames.subList(0, mockedCitiesForCompoundNames.size - 1))
        .containsExactlyInAnyOrder(
            "City, Somewhere",
            "City, Nowhere, Here",
            "City, Where, There",
            "City, Above"
        )
    assertThat(orderedCityNames.last()).isEqualTo("Citi")
  }

  @Test
  fun testCompoundNameScoringEquality() {
    val bonusWeight = 0.1
    val admin2Score = computeScore(LookupQuery("ci, admin2"), mockedCityForEqualScoring, bonusWeight)
    val admin1Score = computeScore(LookupQuery("ci, admin1"), mockedCityForEqualScoring, bonusWeight)

    assertThat(admin2Score).isEqualTo(admin1Score)
  }

  @Test
  fun testScoreWithLatOnly() {
    assertPositionOrder(
        0.0, null,
        "Center", "Northeast", "Northwest", "Southeast", "Southwest"
    )
    assertPositionOrder(
        30.0, null,
        "Northeast", "Northwest", "Center", "Southeast", "Southwest"
    )
    assertPositionOrder(
        90.0, null,
        "Northwest", "Northeast", "Center", "Southeast", "Southwest"
    )
    assertPositionOrder(
        -30.0, null,
        "Southeast", "Southwest", "Center", "Northeast", "Northwest"
    )
    assertPositionOrder(
        -90.0, null,
        "Southwest", "Southeast", "Center", "Northeast", "Northwest"
    )
  }

  @Test
  fun testScoreWithLongOnly() {
    assertPositionOrder(
        null, 0.0,
        "Center", "Northeast", "Northwest", "Southeast", "Southwest"
    )
    assertPositionOrder(
        null, 30.0,
        "Northeast", "Southeast", "Center", "Northwest", "Southwest"
    )
    assertPositionOrder(
        null, 90.0,
        "Southeast", "Northeast", "Center", "Northwest", "Southwest"
    )
    assertPositionOrder(
        null, -30.0,
        "Northwest", "Southwest", "Center", "Northeast", "Southeast"
    )
    assertPositionOrder(
        null, -90.0,
        "Southwest", "Northwest", "Center", "Northeast", "Southeast"
    )
  }

  @Test
  fun testScoreWithLatAndLong() {
    assertPositionOrder(
        0.0, 0.0,
        "Center", "Northeast", "Northwest", "Southeast", "Southwest"
    )
    assertPositionOrder(
        10.0, 5.0,
        "Center", "Northeast", "Northwest", "Southeast", "Southwest"
    )
    assertPositionOrder(
        35.0, -32.0,
        "Northwest", "Center", "Northeast", "Southwest", "Southeast"
    )
    assertPositionOrder(
        -60.0, -10.0,
        "Southwest", "Center", "Southeast", "Northwest", "Northeast"
    )
  }

  private fun assertNameOrder(queryName: String, vararg expectedOrder: String) {
    assertCityOrder(mockedCities, LookupQuery(queryName), { it.name }, *expectedOrder)
  }

  private fun assertCompoundNameOrder(queryName: String, vararg expectedOrder: String) {
    assertCityOrder(mockedCitiesForCompoundNames, LookupQuery(queryName), { getCityNames(it).joinToString(", ") }, *expectedOrder)
  }

  private fun assertPositionOrder(latitude: Double?, longitude: Double?, vararg expectedOrder: String) {
    assertCityOrder(
        mockedCitiesForLocation,
        LookupQuery("City", latitude, longitude),
        { it.country },
        *expectedOrder
    )
  }

  private fun assertCityOrder(
      collection: Collection<City>,
      lookupQuery: LookupQuery,
      cityField: (city: City) -> String,
      vararg expectedOrder: String
  ) {
    assertThat(orderedCityFieldsFromQuery(collection, lookupQuery, cityField)).containsExactly(*expectedOrder)
  }

  private fun orderedCityFieldsFromQuery(
      collection: Collection<City>,
      lookupQuery: LookupQuery,
      cityField: (city: City) -> String
  ): List<String> {
    return scoresCalculator
        .computeScores(lookupQuery, collection)
        .entries
        .sortedByDescending { it.value }
        .map { cityField(it.key) }
  }
}

private val mockedCities = listOf(
    City(
        id = 1,
        name = "City",
        country = "In the Coast",
        latitude = 0.0,
        longitude = 0.0
    ),
    City(
        id = 2,
        name = "City",
        country = "Inland",
        latitude = 45.0,
        longitude = 45.0
    ),
    City(
        id = 3,
        name = "City of Angels",
        country = "United States",
        latitude = 34.0522,
        longitude = 118.2437
    ),
    City(
        id = 4,
        name = "Machi",
        country = "Kuni",
        latitude = 45.0,
        longitude = 22.5
    )
)

private val mockedCitiesForLocation = listOf(
    City(
        id = 1,
        name = "City",
        country = "Center",
        latitude = 0.0,
        longitude = 0.0
    ),
    City(
        id = 2,
        name = "City",
        country = "Northeast",
        latitude = 44.0,
        longitude = 45.0
    ),
    City(
        id = 3,
        name = "City",
        country = "Northwest",
        latitude = 46.0,
        longitude = -47.0
    ),
    City(
        id = 4,
        name = "City",
        country = "Southeast",
        latitude = -50.0,
        longitude = 51.0
    ),
    City(
        id = 5,
        name = "City",
        country = "Southwest",
        latitude = -52.0,
        longitude = -53.0
    )
)

private val mockedCitiesForCompoundNames = listOf(
    City(
        id = 1,
        name = "City",
        admin1name = "Somewhere",
        country = "Country",
        latitude = 0.0,
        longitude = 0.0
    ),
    City(
        id = 2,
        name = "City",
        admin1name = "Nowhere",
        admin2name = "Here",
        country = "Country",
        latitude = 45.0,
        longitude = 45.0
    ),
    City(
        id = 3,
        name = "City",
        admin1name = "Where",
        admin2name = "There",
        country = "Country",
        latitude = 45.0,
        longitude = 45.0
    ),
    City(
        id = 4,
        name = "City",
        admin2name = "Above",
        country = "Country",
        latitude = 45.0,
        longitude = 45.0
    ),
    City(
        id = 5,
        name = "Citi",
        latitude = 45.0,
        longitude = 45.0,
        country = "Country"
    )
)

private val mockedCityForEqualScoring = City(
    id = 1,
    name = "City",
    admin1name = "Admin1",
    admin2name = "Admin2",
    latitude = 45.0,
    longitude = 45.0,
    country = "Country"
)

